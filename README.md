# TA-metricator-for-nmon-insights

Copyright 2017-2018 Octamis - Copyright 2017-2018 Guilhem Marchand

All rights reserved.

## For Splunk Insights for Infrastructure

This addon will deliver the full power of nmon metrics within Splunk Insights for Infrastructure.
Requirements are identical to the regular Technology Addons for Splunk Enterprise.

